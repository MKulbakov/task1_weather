package WeatherController;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.Scanner;

public class CityWeather {
    private String date[] = new String[10];
    private int maxt[] = new int[10];
    private int mint[] = new int[10];
    private String weather[] = new String[10];
    private String city;

    public CityWeather(String mycity) {
        city = mycity;
        this.parse();
    }

    final public String getCity() {
        return city;
    }

    final public String getDate(int i) {return date[i];}

    final public int getMaxt(int i) {return maxt[i];}

    final public int getMint(int i) {return mint[i];}

    public void setMaxt(int maxt, int i) {
        this.maxt[i] = maxt;
    }

    public void setMint(int mint, int i) {
        this.mint[i] = mint;
    }

    final public String getWeather(int i) {return weather[i];}

    final public static int GetCelsius(int f){return (f-32)*5/9;}

    public static String getFieldValue(String s)
    {
        String s1 = s.replaceAll("\"","");
        return s1;
    }

    final public String GetYahoo(){
        String myurl = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22"+city.toLowerCase()+"%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
        String line;
        String result = "";
        try {
            URL url = new URL(myurl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            while ((line = br.readLine()) != null) result += line;
            br.close();
        } catch (Exception e)
        {
            System.out.println("Get Yahoo information failed: "+e);
        }
        return result;
    }

    final public void parse() {
        try {
            String jsonLine = GetYahoo();
            JsonParser parser = new JsonParser();
            JsonObject object = parser.parse(jsonLine).getAsJsonObject();
            object = object.getAsJsonObject("query");
            object = object.getAsJsonObject("results");
            object = object.getAsJsonObject("channel");
            object = object.getAsJsonObject("item");
            JsonArray forecast = object.getAsJsonArray("forecast");
            //PrintStream f = new PrintStream(city+".csv");
            int i = 0;
            for (JsonElement je : forecast){
                JsonObject newobj = je.getAsJsonObject();
                date[i] = getFieldValue(newobj.get("date").toString());
                mint[i] = Integer.parseInt(getFieldValue(newobj.get("low").toString()));
                maxt[i] = Integer.parseInt(getFieldValue(newobj.get("high").toString()));
                weather[i] = getFieldValue(newobj.get("text").toString());
                i++;
            }
        } catch (Exception e){
            System.out.println("Parsing failed: "+e);
        }
    }

    public void toRus(){
        for (int i = 0; i<10; i++)
            if (this.weather[i].toLowerCase().contains("cloud")) {this.weather[i] = "Облачно"; continue;}
            else if (this.weather[i].toLowerCase().contains("sun")) {this.weather[i] = "Солнечно"; continue;}
            else if (this.weather[i].toLowerCase().contains("shower")) {this.weather[i] = "Ливни"; continue;}
            else if (this.weather[i].toLowerCase().contains("rain")) {this.weather[i] = "Дожди"; continue;}
            else if (this.weather[i].toLowerCase().contains("storm")
                     || this.weather[i].toLowerCase().contains("thunder")) {this.weather[i] = "Ураган"; continue;}
            else if (this.weather[i].toLowerCase().contains("breez")) {this.weather[i] = "Бриз"; continue;}
            else if (this.weather[i].toLowerCase().contains("snow")) {this.weather[i] = "Снег"; continue;}
    }

    public void toCelsius(){
        for (int i = 0; i<10; i++)
        {
            this.setMaxt(GetCelsius(this.getMaxt(i)),i);
            this.setMint(GetCelsius(this.getMint(i)),i);
        }
    }

    final public static void SendMail(String to, String myMessage){
        String from = "m.developer@mail.ru";
        String host = "smtp.mail.ru";
        int port = 465;

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.port", port);
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "false");

        Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            @Override
            protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                return new javax.mail.PasswordAuthentication("m.developer@mail.ru", "123qwerty");
            }
        });

        try {
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject("Weather");
            msg.setSentDate(new Date());

            msg.setText(myMessage);

            Transport.send(msg);
        }
        catch (MessagingException mex) {
            mex.printStackTrace();
        }
    }

    final public static ArrayList<String> GetMails(){
        try {
            ArrayList<String> mails = new ArrayList<String>();
            Scanner scanner = new Scanner(new File("dlist.txt"));
            while (scanner.hasNext()){
                mails.add(scanner.nextLine());
            }
            return mails;
        } catch (FileNotFoundException fe){
            System.out.println("File dlist.txt not found!");
            return null;
        }
    }

    public void Send(){
        String mailtext = this.getCity().toUpperCase()+"\n";
        for (int i = 0; i<10; i++)
            mailtext += getDate(i)+";"+getMint(i)+";"+getMaxt(i)+";"+getWeather(i)+"\n";
        ArrayList<String> mails = GetMails();
        for (int i = 0; i<mails.size(); i++)
            SendMail(mails.get(i),mailtext);
    }
}
