package Writers;

import WeatherController.CityWeather;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class XMLWriter extends CityWeather {
    public XMLWriter(String mycity) {
        super(mycity);
    }

    public void Write(){
        try {
            PrintStream f = new PrintStream(System.getProperty("user.dir")+"/WeatherBase/XML/"+this.getCity()+".xml");
            f.println("<?xml version='1.0'?>");
            f.println("<CityWeather>");
            for (int i = 0; i<10; i++){
                f.println("\t<DayWeather>");
                f.println("\t\t<date>"+this.getDate(i)+"</date>");
                f.println("\t\t<mint>"+this.getMint(i)+"</mint>");
                f.println("\t\t<maxt>"+this.getMaxt(i)+"</maxt>");
                f.println("\t\t<weather>"+this.getWeather(i)+"</weather>");
                f.println("\t</DayWeather>");
            }
            f.println("</CityWeather>");
            System.out.println(getCity() + ".xml - Success.");
        } catch (FileNotFoundException e) {
            System.out.println(getCity() + " writing fail : "+e);
        }
    }
}
