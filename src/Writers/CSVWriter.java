package Writers;

import WeatherController.CityWeather;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class CSVWriter extends CityWeather {
    public CSVWriter(String mycity){
        super(mycity);
    }

    public void Write(){
        try {
            PrintStream f = new PrintStream(System.getProperty("user.dir")+"/WeatherBase/CSV/"+this.getCity()+".csv");
            for (int i = 0; i<10; i++)
                f.println(getDate(i)+";"+getMint(i)+";"+getMaxt(i)+";"+getWeather(i));
            System.out.println(getCity() + ".csv - Success.");
        } catch (FileNotFoundException e) {
            System.out.println(getCity() + " writing fail : "+e);
        }
    }
}
