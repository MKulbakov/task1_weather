package Writers;

import WeatherController.CityWeather;

public class Writer{
    public static CSVWriter WriteCSV(CityWeather city, String loc, String tformat){
        CSVWriter mycity = new CSVWriter(city.getCity());
        if (loc == "RU") mycity.toRus();
        if (tformat == "C") mycity.toCelsius();
        mycity.Write();
        mycity.Send();
        return mycity;
    }

    public static XMLWriter WriteXML(CityWeather city, String loc, String tformat){
        XMLWriter mycity = new XMLWriter(city.getCity());
        if (loc == "RU") mycity.toRus();
        if (tformat == "C") mycity.toCelsius();
        mycity.Write();
        mycity.Send();
        return mycity;
    }

    public static JSONWriter WriteJSON(CityWeather city, String loc, String tformat){
        JSONWriter mycity = new JSONWriter(city.getCity());
        if (loc == "RU") mycity.toRus();
        if (tformat == "C") mycity.toCelsius();
        mycity.Write();
        mycity.Send();
        return mycity;
    }
}
