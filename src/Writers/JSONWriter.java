package Writers;

import WeatherController.CityWeather;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class JSONWriter extends CityWeather {
    public JSONWriter(String mycity) {
        super(mycity);
    }

    public void Write(){
        try {
            PrintStream f = new PrintStream(System.getProperty("user.dir")+"/WeatherBase/JSON/"+this.getCity()+".js");
            f.println("{");
            f.println("\t\"cityweather\": [");
            for (int i = 0; i<10; i++)
            {
                f.println("\t{");
                f.println("\t\t\"date\": \""+this.getDate(i)+"\",");
                f.println("\t\t\"mint\": \""+this.getMint(i)+"\",");
                f.println("\t\t\"maxt\": \""+this.getMaxt(i)+"\",");
                f.println("\t\t\"weather\": \""+this.getWeather(i)+"\"");
                if(i<9) f.println("\t},");
                else f.println("\t}");
            }
            f.println("\t]");
            f.println("}");
            System.out.println(getCity() + ".js - Success.");
        } catch (FileNotFoundException e) {
            System.out.println(getCity() + " writing fail : "+e);
        }
    }
}
