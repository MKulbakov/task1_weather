import WeatherController.CityWeather;
import Writers.Writer;

public class Main {
    public static void main(String args[])
    {
        //Хранилища
        CityWeather gomel = new CityWeather("gomel");
        CityWeather moscow = new CityWeather("moscow");
        CityWeather tokyo = new CityWeather("tokyo");
        /*При создании файлов, идущих ниже, данные записываются и отправляются
        на почту в указанном формате*/
        CityWeather city1 = Writer.WriteCSV(gomel,"RU","C");
        CityWeather city2 = Writer.WriteJSON(moscow,"RU","C");
        CityWeather city3 = Writer.WriteXML(tokyo,"EN","F");

    }
}
