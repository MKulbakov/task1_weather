{
	"cityweather": [
	{
		"date": "30 Oct 2018",
		"mint": "28",
		"maxt": "36",
		"weather": "Snow"
	},
	{
		"date": "31 Oct 2018",
		"mint": "27",
		"maxt": "37",
		"weather": "Partly Cloudy"
	},
	{
		"date": "01 Nov 2018",
		"mint": "36",
		"maxt": "46",
		"weather": "Scattered Showers"
	},
	{
		"date": "02 Nov 2018",
		"mint": "39",
		"maxt": "48",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "03 Nov 2018",
		"mint": "40",
		"maxt": "46",
		"weather": "Scattered Showers"
	},
	{
		"date": "04 Nov 2018",
		"mint": "40",
		"maxt": "47",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "05 Nov 2018",
		"mint": "38",
		"maxt": "47",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "06 Nov 2018",
		"mint": "37",
		"maxt": "46",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "07 Nov 2018",
		"mint": "36",
		"maxt": "45",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "08 Nov 2018",
		"mint": "35",
		"maxt": "46",
		"weather": "Mostly Cloudy"
	}
	]
}
