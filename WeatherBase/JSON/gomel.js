{
	"cityweather": [
	{
		"date": "23 Oct 2018",
		"mint": "37",
		"maxt": "49",
		"weather": "Scattered Showers"
	},
	{
		"date": "24 Oct 2018",
		"mint": "37",
		"maxt": "45",
		"weather": "Showers"
	},
	{
		"date": "25 Oct 2018",
		"mint": "35",
		"maxt": "44",
		"weather": "Scattered Showers"
	},
	{
		"date": "26 Oct 2018",
		"mint": "31",
		"maxt": "42",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "27 Oct 2018",
		"mint": "36",
		"maxt": "54",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "28 Oct 2018",
		"mint": "43",
		"maxt": "63",
		"weather": "Cloudy"
	},
	{
		"date": "29 Oct 2018",
		"mint": "46",
		"maxt": "60",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "30 Oct 2018",
		"mint": "42",
		"maxt": "59",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "31 Oct 2018",
		"mint": "38",
		"maxt": "57",
		"weather": "Partly Cloudy"
	},
	{
		"date": "01 Nov 2018",
		"mint": "40",
		"maxt": "59",
		"weather": "Mostly Cloudy"
	}
	]
}
