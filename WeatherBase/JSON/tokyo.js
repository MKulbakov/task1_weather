{
	"cityweather": [
	{
		"date": "23 Oct 2018",
		"mint": "53",
		"maxt": "64",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "24 Oct 2018",
		"mint": "57",
		"maxt": "66",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "25 Oct 2018",
		"mint": "57",
		"maxt": "70",
		"weather": "Sunny"
	},
	{
		"date": "26 Oct 2018",
		"mint": "52",
		"maxt": "68",
		"weather": "Mostly Sunny"
	},
	{
		"date": "27 Oct 2018",
		"mint": "57",
		"maxt": "72",
		"weather": "Scattered Thunderstorms"
	},
	{
		"date": "28 Oct 2018",
		"mint": "53",
		"maxt": "66",
		"weather": "Partly Cloudy"
	},
	{
		"date": "29 Oct 2018",
		"mint": "57",
		"maxt": "69",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "30 Oct 2018",
		"mint": "60",
		"maxt": "70",
		"weather": "Breezy"
	},
	{
		"date": "31 Oct 2018",
		"mint": "56",
		"maxt": "63",
		"weather": "Mostly Cloudy"
	},
	{
		"date": "01 Nov 2018",
		"mint": "52",
		"maxt": "66",
		"weather": "Sunny"
	}
	]
}
